@extends('app')
@section('title', $title)
@section('pagetitle', 'Ilmoita kohde')

@section('content')
    <h1>Luo uusi ilmoitus</h1>
    <img src="http://cdn.osxdaily.com/wp-content/uploads/2013/07/dancing-banana.gif" alt="Banana" style="width:256px;height:256px;">
    <hr/>
    <form action="/403">
        <input type="text" name="otsikko" value="Otsikko" onclick="this.value=''"><br><br>
        <select name="tyyppi">
            <option value="elektroniikka">Elektroniikka</option>
            <option value="autot">Autot</option>
            <option value="huonekalut">Huonekalut</option>
            <option value="vaatteet">Vaatteet</option>
        </select><br><br>
        Kuvausteksti:<br>
        <textarea type="text" name="kuvaus" value="Kuvausteksti" cols="50" rows="10"></textarea><br><br>
        Hinta: <input type="number" step="any" min="0" name="hinta" value="Hinta"><br><br>
        <input type="email" name="yhteystieto" value="Yhteystieto" onclick="this.value=''"><br><br>
        Kuva: <input type="file" name="img"><br><br>
        Voimassa: <input type="date" name="voimassaolo">
        <input type="submit" value="Luo ilmoitus">
    </form>
@endsection