<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>

    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">

    <link href="https://fonts.googleapis.com/css?family=Prompt:100" rel="stylesheet" type="text/css">
    <style>
        html, body {
            height: 100%;
        }

        body {
            margin: 0;
            padding: 0;
            width: 100%;
            display: table;
            font-weight: 100;
            font-family: 'Prompt';
        }

        .container {
            text-align: center;
            display: table-cell;
            vertical-align: middle;
        }

        .content {
            text-align: center;
            display: inline-block;
        }

        .pagetitle {
            font-size: 96px;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="pagetitle">
            @yield('pagetitle')
        </div>
        <div class="content">
            @yield('content')
        </div>
    </div>
</body>
</html>