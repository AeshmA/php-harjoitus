<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ilmoitus extends Model
{
    protected $fillable = [
        'otsikko',
        'kuvausteksti',
        'hinta',
        'yhteystieto',
        'kuva'
    ];
}
