<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'PagesController@koti');
Route::get('koti', 'PagesController@koti');
Route::get('ilmoita', 'PagesController@ilmoita');

Route::get('haku', 'IlmoituksetController@haku');

Route::get('403', 'ErrorController@forbidden');
Route::get('404', 'ErrorController@notfound');
Route::get('503', 'ErrorController@unavailable');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
