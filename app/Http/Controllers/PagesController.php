<?php namespace App\Http\Controllers;

class PagesController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | PagesController
    | Controls routes for different pages
    |--------------------------------------------------------------------------
    */
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application home screen to the user.
     *
     * @return Response
     */

    public function koti()
    {
        $title = 'MAPA koti';
        return view('pages.koti')->with('title', $title);
    }

    public function ilmoita()
    {
        $title = 'Ilmoita';
        return view('pages.ilmoita', compact('title', 'first', 'last'));
    }
}