<?php namespace App\Http\Controllers;

class ErrorController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | ErrorController
    | Controls routes for different errors
    |--------------------------------------------------------------------------
    */
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application home screen to the user.
     *
     * @return Response
     */

    public function forbidden()
    {
        $title = '403';
        return view('errors.403')->with('title', $title);
    }

    public function notfound()
    {
        $title = '404';
        return view('errors.404')->with('title', $title);
    }

    public function unavailable()
    {
        $title = '503';
        return view('errors.503')->with('title', $title);
    }
}