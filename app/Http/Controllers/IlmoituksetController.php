<?php namespace App\Http\Controllers;

class IlmoituksetController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | IlmoituksetController
    |
    |--------------------------------------------------------------------------
    */
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application home screen to the user.
     *
     * @return Response
     */

    public function haku()
    {
        $title = 'Selaa';
        return view('pages.haku')->with('title', $title);
    }
}