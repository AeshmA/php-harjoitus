<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LuoIlmoituksetTaulukko extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ilmoitukset', function (Blueprint $table) {
            $table->increments('id');
            $table->string('otsikko');
            $table->text('kuvausteksti');
            $table->double('hinta');
            $table->string('yhteystieto');
            $table->timestamps();
            $table->timestam('julkaistu');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ilmoitukset');
    }
}
